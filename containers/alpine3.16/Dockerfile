FROM alpine:3.16.0@sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c

ENV TZ=UTC

# renovate: datasource=repology depName=alpine_3_16/apache2 versioning=loose
ENV APACHE_VERSION=2.4.53-r0
# renovate: datasource=repology depName=alpine_3_16/subversion versioning=loose
ENV SUBVERSION_VERSION=1.14.2-r1

RUN set -eux; \
      apk add --no-cache \
        "apache2=${APACHE_VERSION}" \
        "apache2-ldap=${APACHE_VERSION}" \
        "apache2-webdav=${APACHE_VERSION}" \
        "subversion=${SUBVERSION_VERSION}" \
        "mod_dav_svn=${SUBVERSION_VERSION}" \
      ; \
      rm -r \
        /etc/apache2/conf.d \
        /var/www/localhost \
      ; \
      mkdir \
        /var/www/html \
        /etc/apache2/conf.d \
      ; \
      chown apache: /var/www/html;

WORKDIR /var/www/html
STOPSIGNAL SIGWINCH
EXPOSE 80

COPY rootfs /

VOLUME /var/www/html

ENTRYPOINT ["docker-entrypoint"]
CMD ["httpd", "-D", "FOREGROUND"]
HEALTHCHECK \
  --start-period=180s \
  --timeout=5s \
  --interval=30s \
  --retries=6 \
  CMD ["wget", "-q", "-O", "-", "http://localhost:80/!svn/healthcheck"]
